@echo off

echo Recordar: export CLASSPATH="./antlr4.jar"

echo GENERANDO LEXER Y PARSER EN JAVA...
java -jar ../antlr-4.7.2-complete.jar %1.g4

echo COMPILANDO ARCHIVOS JAVA...

javac -g *.java

echo GENERANDO ARBOL...
java -cp ".;../antlr-4.7.2-complete.jar" org.antlr.v4.gui.TestRig %1 %2 -gui %3

echo LIMPIANDO ARCHIVOS...
del *.java
del *.class