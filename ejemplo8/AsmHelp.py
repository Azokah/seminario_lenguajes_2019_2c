# https://schweigi.github.io/assembler-simulator/
# Posicion 232 es donde esta el buffer de video

class AsmHelper():

    asm_program = []

    asm_data = []
    data_counter = 0
    label_counter = 0

    def get_program(self):
        program = 'JMP start\n'

        for line in self.asm_data:
            program += line + '\n'

        program += '''
print:			; print(C:*from, D:*to)
	MOV B, 0
.loop:
	MOV A, [C]	; Get char from var
	MOV [D], A	; Write to output
	INC C
	INC D  
	CMP B, [C]	; Check if end
	JNZ .loop	; jump if not

	RET

start:
        '''
        for line in self.asm_program:
            program += line + '\n'

        program += 'HLT'

        return program

    def print(self, text):
        self.data_counter += 1

        self.asm_data.append('''
data{}: DB "{}"
        DB 0
        '''.format(self.data_counter, text))

        self.asm_program.append('''
    MOV C, data{}    ; Point to var 
    MOV D, 232	; Point to output
    CALL print        
        '''.format(self.data_counter))

    # '=='|'!='|'>='|'>'|'<='|'<'
    def jump(self, operator):
        if operator == '!=':
            return 'JE'
        elif operator == '==':
            return 'JNE'
        elif operator == '<=':
            return 'JA'      # JG
        elif operator == '<':
            return 'JAE'     # JGE
        elif operator == '>=':
            return 'JB'      # JL
        elif operator == '>':
            return 'JBE'     # JLE

    def if_statement(self, nro1, operator, nro2):
        self.label_counter += 1 
        self.asm_program.append('CMP {}, {}'.format(nro1, nro2))
        self.asm_program.append(self.jump(operator) + ' label{}'.format(self.label_counter))
        
        # EN EL VISITADOR DEL NODO IF:
        #   agregar codigo del if
        #   agregar etiqueta

        return 'label{}'.format(self.label_counter)


asm = AsmHelper()
asm.print("Hola mundo")
asm.if_statement(3, '==', 5)

print(asm.get_program())