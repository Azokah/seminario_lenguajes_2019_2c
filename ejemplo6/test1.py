import antlr4
from antlr4 import CommonTokenStream, ParseTreeWalker, InputStream

from AritmeticaLexer import AritmeticaLexer
from AritmeticaParser import AritmeticaParser
from AritmeticaListener import AritmeticaListener

class PrintListener(AritmeticaListener):

    def enterStatement(self, ctx:AritmeticaParser.StatementContext):
        print('entra statement')

    def exitStatement(self, ctx:AritmeticaParser.StatementContext):
        print('sale statement')

    def enterExpression(self, ctx:AritmeticaParser.ExpressionContext):
        print('entra expression')

    def exitExpression(self, ctx:AritmeticaParser.ExpressionContext):
        print('sale expression')

    def enterTerm(self, ctx:AritmeticaParser.TermContext):
        print('entra term')

    def exitTerm(self, ctx:AritmeticaParser.TermContext):
        print('sale term')

    def enterFactor(self, ctx:AritmeticaParser.FactorContext):
        print('entra factor')

    def exitFactor(self, ctx:AritmeticaParser.FactorContext):
        print('sale factor')
        for nodo in ctx.children:
            if type(nodo) == antlr4.tree.Tree.TerminalNodeImpl:
                print(nodo)



expr = '3+5*2'


print('Comenzando...')
input = InputStream(expr)
lexer = AritmeticaLexer(input)
stream = CommonTokenStream(lexer)
parser = AritmeticaParser(stream)

tree = parser.statement()

nuestroListener = PrintListener()
walker = ParseTreeWalker()
walker.walk(nuestroListener, tree)

print('Fin.')