grammar Seminario1;

program:
    (statement)+
    ;

statement :
      regla_saludar
    | reglar_insultar
    ;

regla_saludar : 
    'saludar ' NOMBRE {print('hola ' + $NOMBRE.text) }
    ;

reglar_insultar :
    'insultar ' NOMBRE {print('&/#% ' + $NOMBRE.text) }
    ;

NOMBRE : [a-zA-Z]+;

GARBAGE : [ \r\n\t] -> skip;
