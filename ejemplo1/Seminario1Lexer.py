# Generated from Seminario1.g4 by ANTLR 4.7.2
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\6")
        buf.write("\'\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\3\2\3\2\3\2\3\2")
        buf.write("\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3")
        buf.write("\3\3\3\3\4\6\4 \n\4\r\4\16\4!\3\5\3\5\3\5\3\5\2\2\6\3")
        buf.write("\3\5\4\7\5\t\6\3\2\4\4\2C\\c|\5\2\13\f\17\17\"\"\2\'\2")
        buf.write("\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\3\13\3")
        buf.write("\2\2\2\5\24\3\2\2\2\7\37\3\2\2\2\t#\3\2\2\2\13\f\7u\2")
        buf.write("\2\f\r\7c\2\2\r\16\7n\2\2\16\17\7w\2\2\17\20\7f\2\2\20")
        buf.write("\21\7c\2\2\21\22\7t\2\2\22\23\7\"\2\2\23\4\3\2\2\2\24")
        buf.write("\25\7k\2\2\25\26\7p\2\2\26\27\7u\2\2\27\30\7w\2\2\30\31")
        buf.write("\7n\2\2\31\32\7v\2\2\32\33\7c\2\2\33\34\7t\2\2\34\35\7")
        buf.write("\"\2\2\35\6\3\2\2\2\36 \t\2\2\2\37\36\3\2\2\2 !\3\2\2")
        buf.write("\2!\37\3\2\2\2!\"\3\2\2\2\"\b\3\2\2\2#$\t\3\2\2$%\3\2")
        buf.write("\2\2%&\b\5\2\2&\n\3\2\2\2\4\2!\3\b\2\2")
        return buf.getvalue()


class Seminario1Lexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    T__1 = 2
    NOMBRE = 3
    GARBAGE = 4

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'saludar '", "'insultar '" ]

    symbolicNames = [ "<INVALID>",
            "NOMBRE", "GARBAGE" ]

    ruleNames = [ "T__0", "T__1", "NOMBRE", "GARBAGE" ]

    grammarFileName = "Seminario1.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


