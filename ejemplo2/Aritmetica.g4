grammar Aritmetica;
// agregar: resta, dividir, potencia

statement:
    e=expression                {print('Result: ' + str($e.value))}
    ;

expression returns [int value]: 
      t=term                    {$value = $t.value}
    | e=expression '+' t=term   {$value = $e.value + $t.value }
    | e=expression '-' t=term   {$value = $e.value - $t.value }
    ;

term returns [int value]:
      f=factor                  {$value = $f.value}
    | t=term '*' f=factor       {$value = $t.value * $f.value}
    | t=term '/' f=factor       {$value = $t.value / $f.value}
    | t=term '^' f=factor       {$value = $t.value ** $f.value}
    ;

factor returns [int value]:
      NUMBER                    {$value = int($NUMBER.text)}
    | '(' e=expression ')'      {$value = $e.value}
    ;

NUMBER : DIGIT+;
DIGIT  : [0-9];

WS : [ \r\n\t] -> skip;
