from antlr4 import CommonTokenStream, ParseTreeWalker, InputStream

from AritmeticaLexer import AritmeticaLexer
from AritmeticaParser import AritmeticaParser
from AritmeticaListener import AritmeticaListener


print('Comenzando...')
input = InputStream('2^5')
lexer = AritmeticaLexer(input)
stream = CommonTokenStream(lexer)
parser = AritmeticaParser(stream)

tree = parser.statement()


print('Fin.')