from antlr4 import CommonTokenStream, ParseTreeWalker, InputStream

from JSONLexer import JSONLexer
from JSONParser import JSONParser
from JSONListener import JSONListener

js = '''
{ "prop1": 1,
    "prop2": {
        "prop22": 2,
        "prop23": "tres"
    },
    "prop11": "once"
}
'''

print('Comenzando...')
input = InputStream(js)
lexer = JSONLexer(input)
stream = CommonTokenStream(lexer)
parser = JSONParser(stream)

tree = parser.json()


print('Fin.')