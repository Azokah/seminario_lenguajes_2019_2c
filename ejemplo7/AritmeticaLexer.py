# Generated from Aritmetica.g4 by ANTLR 4.7.2
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\25")
        buf.write("r\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\4\3\4\3\5")
        buf.write("\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\b\3\b\3")
        buf.write("\b\3\t\3\t\3\n\3\n\3\13\3\13\3\f\3\f\3\r\3\r\3\16\3\16")
        buf.write("\3\17\3\17\3\20\6\20R\n\20\r\20\16\20S\3\20\7\20W\n\20")
        buf.write("\f\20\16\20Z\13\20\3\21\3\21\3\21\3\21\3\21\3\21\3\21")
        buf.write("\3\21\3\21\3\21\5\21f\n\21\3\22\6\22i\n\22\r\22\16\22")
        buf.write("j\3\23\3\23\3\24\3\24\3\24\3\24\2\2\25\3\3\5\4\7\5\t\6")
        buf.write("\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20")
        buf.write("\37\21!\22#\23%\24\'\25\3\2\6\4\2C\\c|\5\2\62;C\\c|\3")
        buf.write("\2\62;\5\2\13\f\17\17\"\"\2y\2\3\3\2\2\2\2\5\3\2\2\2\2")
        buf.write("\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3")
        buf.write("\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2")
        buf.write("\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2")
        buf.write("\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\3)\3\2")
        buf.write("\2\2\5.\3\2\2\2\7\61\3\2\2\2\t\63\3\2\2\2\13\66\3\2\2")
        buf.write("\2\r<\3\2\2\2\17?\3\2\2\2\21B\3\2\2\2\23D\3\2\2\2\25F")
        buf.write("\3\2\2\2\27H\3\2\2\2\31J\3\2\2\2\33L\3\2\2\2\35N\3\2\2")
        buf.write("\2\37Q\3\2\2\2!e\3\2\2\2#h\3\2\2\2%l\3\2\2\2\'n\3\2\2")
        buf.write("\2)*\7h\2\2*+\7t\2\2+,\7q\2\2,-\7o\2\2-\4\3\2\2\2./\7")
        buf.write("v\2\2/\60\7q\2\2\60\6\3\2\2\2\61\62\7A\2\2\62\b\3\2\2")
        buf.write("\2\63\64\7\60\2\2\64\65\7A\2\2\65\n\3\2\2\2\66\67\7y\2")
        buf.write("\2\678\7j\2\289\7k\2\29:\7n\2\2:;\7g\2\2;\f\3\2\2\2<=")
        buf.write("\7\60\2\2=>\7\60\2\2>\16\3\2\2\2?@\7>\2\2@A\7/\2\2A\20")
        buf.write("\3\2\2\2BC\7-\2\2C\22\3\2\2\2DE\7/\2\2E\24\3\2\2\2FG\7")
        buf.write(",\2\2G\26\3\2\2\2HI\7\61\2\2I\30\3\2\2\2JK\7`\2\2K\32")
        buf.write("\3\2\2\2LM\7*\2\2M\34\3\2\2\2NO\7+\2\2O\36\3\2\2\2PR\t")
        buf.write("\2\2\2QP\3\2\2\2RS\3\2\2\2SQ\3\2\2\2ST\3\2\2\2TX\3\2\2")
        buf.write("\2UW\t\3\2\2VU\3\2\2\2WZ\3\2\2\2XV\3\2\2\2XY\3\2\2\2Y")
        buf.write(" \3\2\2\2ZX\3\2\2\2[\\\7?\2\2\\f\7?\2\2]^\7#\2\2^f\7?")
        buf.write("\2\2_`\7@\2\2`f\7?\2\2af\7@\2\2bc\7>\2\2cf\7?\2\2df\7")
        buf.write(">\2\2e[\3\2\2\2e]\3\2\2\2e_\3\2\2\2ea\3\2\2\2eb\3\2\2")
        buf.write("\2ed\3\2\2\2f\"\3\2\2\2gi\5%\23\2hg\3\2\2\2ij\3\2\2\2")
        buf.write("jh\3\2\2\2jk\3\2\2\2k$\3\2\2\2lm\t\4\2\2m&\3\2\2\2no\t")
        buf.write("\5\2\2op\3\2\2\2pq\b\24\2\2q(\3\2\2\2\7\2SXej\3\b\2\2")
        return buf.getvalue()


class AritmeticaLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    T__6 = 7
    T__7 = 8
    T__8 = 9
    T__9 = 10
    T__10 = 11
    T__11 = 12
    T__12 = 13
    T__13 = 14
    VARIABLE = 15
    COMPARATION_OPERATOR = 16
    NUMBER = 17
    DIGIT = 18
    WS = 19

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'from'", "'to'", "'?'", "'.?'", "'while'", "'..'", "'<-'", 
            "'+'", "'-'", "'*'", "'/'", "'^'", "'('", "')'" ]

    symbolicNames = [ "<INVALID>",
            "VARIABLE", "COMPARATION_OPERATOR", "NUMBER", "DIGIT", "WS" ]

    ruleNames = [ "T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", 
                  "T__7", "T__8", "T__9", "T__10", "T__11", "T__12", "T__13", 
                  "VARIABLE", "COMPARATION_OPERATOR", "NUMBER", "DIGIT", 
                  "WS" ]

    grammarFileName = "Aritmetica.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


