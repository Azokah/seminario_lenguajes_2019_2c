#-*- coding: utf-8 -*-

import antlr4
from antlr4 import CommonTokenStream, ParseTreeWalker, InputStream

from AritmeticaLexer import AritmeticaLexer
from AritmeticaParser import AritmeticaParser
from AritmeticaListener import AritmeticaListener

class Visitador(AritmeticaListener):

    def __init__(self):
        self.variables = {}

    def enterProgram(self, ctx:AritmeticaParser.ProgramContext):
        for nodo in ctx.children:
            self.visitStatement(nodo)

    def visitStatement(self, ctx:AritmeticaParser.StatementContext):
        if type(ctx.children[0]) == AritmeticaParser.ExpressionContext:
            valor = self.visitExpression(ctx.children[0])
            print(valor)
        elif type(ctx.children[0]) == AritmeticaParser.If_statementContext:
            self.visitIf_statement(ctx.children[0])
        elif type(ctx.children[0]) == AritmeticaParser.Assign_statementContext:
            self.visitAssignStatement(ctx.children[0])
        elif type(ctx.children[0]) == AritmeticaParser.While_statementContext:
            self.visitWhileStatement(ctx.children[0])
        elif type(ctx.children[0]) == AritmeticaParser.For_statementContext:
            self.visitForStatement(ctx.children[0])
        else:
            print(type(ctx)) # No deberia llegar nunca

    def visitForStatement(self, ctx:AritmeticaParser.For_statementContext):
        for statement in ctx.children[5:-1]:
            for x in range(int(ctx.a.text), int(ctx.b.text)):
                self.visitStatement(statement)

    def visitWhileStatement(self, ctx:AritmeticaParser.While_statementContext):
        conditionValue = self.visitBooleanexpression(ctx.children[1])
        while conditionValue:
            for nodo in ctx.children[3:-1]:
                self.visitStatement(nodo)

            conditionValue = self.visitBooleanexpression(ctx.children[1])

    def visitAssignStatement(self, ctx:AritmeticaParser.Assign_statementContext):
        value = self.visitExpression(ctx.children[2])
        self.variables[ctx.children[0].symbol.text] = value

    def visitIf_statement(self, ctx:AritmeticaParser.If_statementContext):
        logicValue = self.visitBooleanexpression(ctx.children[1])
        if logicValue:
            for nodo in ctx.children[3:-1]:  # Desde la 4ta posic hasta la anteultima
                self.visitStatement(nodo)

    def visitBooleanexpression(self, ctx:AritmeticaParser.BooleanexpressionContext):
        value1 = self.visitExpression(ctx.children[0])
        value2 = self.visitExpression(ctx.children[2])
        op = ctx.children[1].symbol.text
        return eval(str(value1) + op + str(value2))

    def visitExpression(self, ctx:AritmeticaParser.ExpressionContext):
        valor = 0
        if type(ctx.children[0]) == AritmeticaParser.TermContext:
            valor = self.visitTerm(ctx.children[0])
        elif type(ctx.children[0]) == AritmeticaParser.ExpressionContext:
            valor1 = self.visitExpression(ctx.children[0])
            valor2 = self.visitTerm(ctx.children[2])
            if ctx.children[1].symbol.text == "+":
                valor = valor1 + valor2
            else:
                valor = valor1 - valor2

        return valor

    def visitTerm(self, ctx:AritmeticaParser.TermContext):
        valor = 0
        if type(ctx.children[0]) == AritmeticaParser.FactorContext:
            valor = self.visitFactor(ctx.children[0])
        else:
            valor1 = self.visitTerm(ctx.children[0])
            valor2 = self.visitFactor(ctx.children[2])
            if ctx.children[1].symbol.text == "*":
                valor = valor1 * valor2
            elif ctx.children[1].symbol.text == "/":
                valor = valor1 / valor2
            else:
                valor = valor1 ** valor2

        return valor

    def visitFactor(self, ctx:AritmeticaParser.FactorContext):
        valor = 0
        
        if len(ctx.children) == 1:

            if ctx.var:
                valor = self.variables[ctx.var.text]
            else:
                valor = int(ctx.num.text)

        else:
            valor = self.visitExpression(ctx.children[1])

        return valor

expr = '''
a <- 2
a <- a*3
while a < 10 ?
a <- a + 1
a
.?
from 50 to 60 ?
a
.?
'''


print('Comenzando...')
input = InputStream(expr)
lexer = AritmeticaLexer(input)
stream = CommonTokenStream(lexer)
parser = AritmeticaParser(stream)

tree = parser.program()

nuestroListener = Visitador()
walker = ParseTreeWalker()
walker.walk(nuestroListener, tree)

print('Fin.')