grammar Aritmetica;

program:
    (statement)+
    ;

statement:
      expression
    | if_statement
    | assign_statement
    | while_statement
    | for_statement
    ;

for_statement:
    'from' a=NUMBER 'to' b=NUMBER '?' (statement)+ '.?'
    ;

while_statement:
    'while' booleanexpression '?' (statement)+ '.?'
    ;

if_statement:
    '..' booleanexpression '?' (statement)+ '.?'
    ;

assign_statement:
    VARIABLE '<-' expression
    ;

booleanexpression:
    expression COMPARATION_OPERATOR expression
    ;

expression:
      term
    | expression '+' term
    | expression '-' term
    ;

term:
      factor
    | term ('*'|'/'|'^') factor
    ;

factor:
      num=NUMBER
    | var=VARIABLE
    | '(' expression ')'
    ;

VARIABLE: [a-zA-Z]+[a-zA-Z0-9]*;

COMPARATION_OPERATOR: '=='|'!='|'>='|'>'|'<='|'<' ;

NUMBER : DIGIT+;
DIGIT  : [0-9];

WS : [ \r\n\t] -> skip;
